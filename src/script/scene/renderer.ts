import { WebGLRenderer } from 'three';

const container = document.getElementById('renderer');

class Renderer extends WebGLRenderer {
    constructor(props) {
        super(props);
        this.setClearColor(0x000000);

        container.innerHTML = "";
        container.appendChild(this.domElement);

        window.addEventListener('resize', this.onResize.bind(this));
        this.onResize();
    }

    onResize() {
        let w = window.innerWidth;
        let h = window.innerHeight;
        this.setSize(w, h);
        this.setViewport(0, 0, w, h);
        this.setPixelRatio(window.devicePixelRatio);
    }
}

export default Renderer;
