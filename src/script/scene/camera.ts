import { PerspectiveCamera, Vector3 } from 'three';
import TrackballControls = require('three-trackballcontrols');

class Camera extends PerspectiveCamera {

    //private controls:TrackballControls;

    constructor() {
        super();

        this.aspect = window.innerWidth / window.innerHeight;
        this.near = 1;
        this.far = 1000;
        this.position.set(10, -10, 10)
        this.fov = 60;
        this.up = new Vector3(0, 0, 1);

        this.lookAt(new Vector3(0, 0, 0));

        //this.controls = new TrackballControls(this);

        window.addEventListener('resize', (e) => { this.onResize(e); });
        this.onResize(this.onResize);
    }

    render() {
        //this.controls.update();
        this.lookAt(new Vector3(0, 0, 0), 1);
    }

    onResize(e) {
        this.aspect = window.innerWidth / window.innerHeight;
        this.updateProjectionMatrix();
    }
}

export default Camera;
