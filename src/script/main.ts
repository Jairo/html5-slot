import { Scene, Object3D, AxesHelper, Vector3 } from 'three';
import Stats = require('stats-js');
import TWEEN = require('@tweenjs/tween.js');

import Camera from './scene/camera';
import Renderer from './scene/renderer';
import Terrain from './elements/terrain';
import Text from './elements/text';

class Main {
    private renderTargets = [];
    private scene:Scene;
    private rootNode:Object3D;
    private camera:Camera;
    private renderer:Renderer;
    private stats:Stats;

    private step:number = 0;
    private text:Text; 

    constructor () {
      this.setupScene();
      this.setupElements();
      this.animationLoop();
      this.addEventListeners();
    }

    animationLoop() {
        requestAnimationFrame((time) => {
            TWEEN.update(time);
            this.animationLoop();
        });

        this.stats.begin();
        this.render();
        this.stats.end();
    }

    setupScene() {
        this.scene = new Scene();

        this.rootNode = new Object3D();
        this.scene.add(this.rootNode);

        this.camera = new Camera();
        this.rootNode.add(this.camera);
        this.renderTargets.push(this.camera);

        this.scene.add(new AxesHelper(5));

        this.renderer = new Renderer({
            alpha: true,
            antialias: true,
            devicePixelRatio: window.devicePixelRatio || 1
        });

        this.stats = new Stats();
        this.stats.setMode(0);
        document.body.appendChild(this.stats.domElement);
    }
    
    render() {
        for (let i = 0; i < this.renderTargets.length; i++) {
            this.renderTargets[i].render();
        }
        this.renderer.render(this.scene, this.camera);
    }
    
    addRenderTarget(target) {
        this.renderTargets.push(target);
    }
    
    removeRenderTarget(target) {
        let index = this.renderTargets.indexOf(target);
        if (index > -1) this.renderTargets.splice(index, 1);
    }
    
    setupElements() {
        let terrain = new Terrain(10, 10, 149, 149);
        this.addRenderTarget(terrain);
        this.rootNode.add(terrain);
    }

    addEventListeners() {
        document.addEventListener('click', this.handleClick.bind(this));
    }

    removeEventListeners() {
        document.removeEventListener('click', this.handleClick.bind(this));
    }

    handleClick() {
        if (this.step++ < 5) {
            let target = new Object3D().copy(this.camera);
            target.translateZ(-2);
            let t = new TWEEN.Tween(this.camera.position)
                                .to({x: target.position.x, y: target.position.y, z: target.position.z}, 240)
                                .easing(TWEEN.Easing.Quadratic.InOut);
            t.start();
        }
        
        if (this.step == 5) {
            this.text = new Text('THE END');
            this.text.position.set(0, 0, 3.5);
            this.text.scale.set(.07, .07, .07);
            this.addRenderTarget(this.text);
            this.rootNode.add(this.text);

            this.text.up = new Vector3(0, 0, 1);
            this.text.lookAt(this.camera.position);

            let t = new TWEEN.Tween(this.text.material)
                                .to({opacity: 1}, 240)
                                .delay(480)
                                .easing(TWEEN.Easing.Quadratic.In);
            t.start();

            this.removeEventListeners();
        }
    }
}

export default Main;
new Main();
