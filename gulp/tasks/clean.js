var gulp = require('gulp');
var del = require('del');
var PATH = require('../paths.js');

gulp.task('clean', function(callback) {
	del([PATH.BUILD + '/**/*'], callback);
});