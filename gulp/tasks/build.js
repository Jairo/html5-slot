var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var tsify = require('tsify');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');

gulp.task('copy-html', function () {
    return gulp.src(['src/html/*.html'])
        .pipe(gulp.dest('build'));
});

gulp.task('copy-css', function () {
    return gulp.src(['src/html/css/*.css'])
        .pipe(gulp.dest('build'));
});

gulp.task('bundle', function () {
    return browserify({
        basedir: '.',
        debug: true,
        entries: ['src/script/main.ts'],
        cache: {},
        packageCache: {}
    })
    .plugin(tsify)
    .transform('babelify', {
        presets: ['es2015'],
        extensions: ['.ts']
    })
    .bundle()
    .pipe(source('main.js'))
    .pipe(buffer())
	.pipe(uglify())
    .pipe(gulp.dest('build'));
});

gulp.task('build', ['clean', 'copy-html', 'bundle']);